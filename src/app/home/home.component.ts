import { Component, OnInit } from '@angular/core';
import { SliderService } from '../service/slider.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public loanPeriod: number = 6;
  public loanAmount: number = 2000000;
  public total: number;

  constructor(
    private sliderService: SliderService
  ) { }

  ngOnInit(): void {
  }

  formatRp() {
    
  }

  returnTotal () {
    this.total = Math.ceil(this.sliderService.getTotal());
    let reverse = this.total.toString().split('').reverse().join(''),
      ribuan = reverse.match(/\d{1,3}/g);
      let price = ribuan.join('.').split('').reverse().join('');
   return 'Rp ' + price;
    // return Math.ceil(this.total);
  }
}

