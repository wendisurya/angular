import { Component, OnInit } from '@angular/core';
import { SliderService } from '../service/slider.service'

@Component({
  selector: 'app-loan-detail',
  templateUrl: './loan-detail.component.html',
  styleUrls: ['./loan-detail.component.scss']
})
export class LoanDetailComponent implements OnInit {
  loanData = [] 
  constructor(
    private sliderService: SliderService
  ) { }

  ngOnInit(): void {
    this.loanData = this.sliderService.getLoanData();
    console.log(this.loanData, typeof this.loanData);
    
  }

}
