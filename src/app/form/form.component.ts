import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

import { SliderService } from '../service/slider.service'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  loanForm: FormGroup;
  genderKTP : string = 'Male';
  dobKTP : string = '';
  checkArray = [];
  submit = false;
  reasons : Array<any> = [
    { name: 'Investasi', value: 'investasi' },
    { name: 'Liburan', value: 'liburan' },
    { name: 'Lainnya', value: 'Lainnya' }
  ];

  constructor(
    private sliderService: SliderService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.loanForm = this.formBuilder.group({
      name: ['', Validators.required],
      ktp: ['', [Validators.required, Validators.minLength(16)]],
      gender: [this.genderKTP],
      dob: ['', Validators.required],
      alamat: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(13)]],
      reason: [[]],
      period: [this.sliderService.getPeriod()],
      amount: [this.sliderService.getAmount() / 1000000],
      total: [Math.ceil(this.sliderService.getTotal())]
    })
    console.log(typeof this.reasons);
    
  }

  returnLoanAmount () {
     return this.sliderService.getAmount() / 1000000;
  }

  returnLoanPeriod () {
    return this.sliderService.getPeriod();
  }

  returnLoanTotal () {
    let total = Math.ceil( this.sliderService.getTotal()); 
    let reverse = total.toString().split('').reverse().join(''),
    ribuan = reverse.match(/\d{1,3}/g);
    let price = ribuan.join('.').split('').reverse().join('');
    return 'Rp ' + price;
  }

  numericalInputOnly(event: KeyboardEvent): boolean {
    const e = event.which || event.keyCode;
    const isNumber = e < 58 && e > 47;
  
    if (isNumber) {
      return true;
    }
  
    return false;
  }
  

  get error() {
    return this.loanForm.controls
  }

  checkKTP(event) {
    let nomorNIK = event.target.value;
    
    if (nomorNIK.length == 16) {
        let thisYear = new Date().getFullYear().toString().substr(-2);


        let thisDate = {
            hari: (nomorNIK.substr(6, 2) > 40) ? nomorNIK.substr(6, 2) - 40 : nomorNIK.substr(6, 2),
            bulan: nomorNIK.substr(8, 2),
            tahun: (nomorNIK.substr(10, 2) > 1 && nomorNIK.substr(10, 2) < thisYear) ? "20" + nomorNIK.substr(10, 2) : "19" + nomorNIK.substr(10, 2)
        }

        if (nomorNIK.substr(6, 2) > 40 && nomorNIK.substr(6, 2) - 40 < 10) {
          let day = nomorNIK.substr(6, 2) - 40;
          thisDate.hari = "0" + day.toString()
        }

        let gender = (nomorNIK.substr(6, 2) > 40 && nomorNIK.substr(6,2) <=71) ? 'Female' : 'Male';
        this.genderKTP = gender;
        this.loanForm.controls['gender'].setValue(gender);
        this.loanForm.controls['dob'].setValue(thisDate.tahun + "-" + thisDate.bulan + "-" + thisDate.hari);
  }
}

onCheckboxChange(e) {
  if (e.target.checked) {
    this.checkArray.push(e.target.value);
      this.loanForm.controls['reason'].setValue(this.checkArray)
  } else {
      this.checkArray.splice(this.checkArray.indexOf(e.target.value), 1)
      this.loanForm.controls['reason'].setValue(this.checkArray)
  } 
}

onModalClose() {
  window.location.href = "/"
}

directToDetails() {
  const myElements = document.getElementsByClassName("modal-backdrop")[0];
  myElements.classList.add('hidden')
}

  onSubmit (e) {
    this.sliderService.setLoanData(this.loanForm.value);
  }
}
